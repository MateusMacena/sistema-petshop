package com.danielabella.rest.exemplo3.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.danielabella.rest.exemplo3.domain.Tracking;

@Repository
public interface TrackingRepository extends JpaRepository<Tracking,Integer>{

}
