package com.danielabella.rest.exemplo3.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.danielabella.rest.exemplo3.domain.Tracking;
import com.danielabella.rest.exemplo3.service.TrackingService;

/**
*
* @author Mateus
*
*/

@RestController
public class TrackingController {
	
	@Autowired
	private TrackingService service;

	
	@RequestMapping(value = "/tracking/{codtracking}", method = RequestMethod.GET)
	public ResponseEntity<Tracking> obterTracking(@PathVariable int codtracking) {

		Tracking tracking = service.obterTrackingPorCodigo(codtracking);
		if (tracking == null) {
			return new ResponseEntity<Tracking>(HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<Tracking>(tracking, HttpStatus.OK);
		}
}
	

	@RequestMapping(value = "/tracking", method = RequestMethod.POST)
	public ResponseEntity<Tracking> inserirNovoTracking(@RequestBody Tracking registro) {
	
try {
			
	Tracking registroInserido = service.criarTracking(registro);
			
	if(registroInserido != null) {
				return new ResponseEntity<Tracking>(registroInserido, HttpStatus.CREATED);
			} else {
				return new ResponseEntity<Tracking>(HttpStatus.UNAUTHORIZED);
			}

		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Tracking>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
	
	@RequestMapping("/tracking")
	public ResponseEntity<List<Tracking>> listarTracking() {
		return new ResponseEntity<List<Tracking>>(service.ListarRastreamentos(), HttpStatus.OK);

	}
	
	@RequestMapping("/tracking/{id}")
	public ResponseEntity<Tracking> deleteTracking(@PathVariable(value = "id") int id_track) {
		service.deletarTracking(id_track);
		return new ResponseEntity<Tracking>(HttpStatus.OK);
	}
	
	
	@RequestMapping("/trackingup")
	public ResponseEntity<Tracking> updateTracking(@RequestBody Tracking tracking) {
		service.atualizarTracking(tracking);
		return new ResponseEntity<Tracking>(HttpStatus.OK);
	}
	
}
