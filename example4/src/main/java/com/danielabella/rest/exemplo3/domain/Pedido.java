package com.danielabella.rest.exemplo3.domain;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
*
* @author Mateus
*
*/

@Entity
public class Pedido {
	
	
	private int cod_pedido;
	private int cod_cliente;
	private int cod_tipo_pagto;
	private double valor_total;
	private int cod_endereco_cobranca;
	private int cod_endereco_entrega;
	private int cod_status;
	private Timestamp data_status;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getCod_pedido() {
		return cod_pedido;
	}
	public void setCod_pedido(int cod_pedido) {
		this.cod_pedido = cod_pedido;
	}
	public int getCod_cliente() {
		return cod_cliente;
	}
	public void setCod_cliente(int cod_cliente) {
		this.cod_cliente = cod_cliente;
	}
	public int getCod_tipo_pagto() {
		return cod_tipo_pagto;
	}
	public void setCod_tipo_pagto(int cod_tipo_pagto) {
		this.cod_tipo_pagto = cod_tipo_pagto;
	}
	public double getValor_total() {
		return valor_total;
	}
	public void setValor_total(double valor_total) {
		this.valor_total = valor_total;
	}
	public int getCod_endereco_cobranca() {
		return cod_endereco_cobranca;
	}
	public void setCod_endereco_cobranca(int cod_endereco_cobranca) {
		this.cod_endereco_cobranca = cod_endereco_cobranca;
	}
	public int getCod_endereco_entrega() {
		return cod_endereco_entrega;
	}
	public void setCod_endereco_entrega(int cod_endereco_entrega) {
		this.cod_endereco_entrega = cod_endereco_entrega;
	}
	public int getCod_status() {
		return cod_status;
	}
	public void setCod_status(int cod_status) {
		this.cod_status = cod_status;
	}
	public Timestamp getData_status() {
		return data_status;
	}
	public void setData_status(Timestamp data_status) {
		this.data_status = data_status;
	}
	
	
	

}
