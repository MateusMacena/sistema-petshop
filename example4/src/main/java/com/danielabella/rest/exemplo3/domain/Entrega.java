package com.danielabella.rest.exemplo3.domain;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;



/**
 *
 * @author Mateus

 *
 */

/**
 * 
 * O @Data serve para gerar os Getters e Setters
 *
 */
@Data
@Entity
public class Entrega implements Serializable {
	
	private static final long serialVersionUID = -7799369695818057571L;

	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int cod_entrega;
	
	private int cod_pedido;
	private int previsto_para;
	private int cod_status;
	private Timestamp data_status;
	
	
}
