package com.danielabella.rest.exemplo3.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.danielabella.rest.exemplo3.domain.Pedido;
import com.danielabella.rest.exemplo3.service.PedidoService;

/**
*
* @author Mateus
*
*/

@RestController
public class PedidoController {
	
	@Autowired
	private PedidoService service;

	
	@RequestMapping(value = "/pedido/{codpedido}", method = RequestMethod.GET)
	public ResponseEntity<Pedido> obterPedido(@PathVariable int codpedido) {

		Pedido pedido = service.obterPedidoPorCodigo(codpedido);
		if (pedido == null) {
			return new ResponseEntity<Pedido>(HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<Pedido>(pedido, HttpStatus.OK);
		}
}
	

	@RequestMapping(value = "/pedido", method = RequestMethod.POST)
	public ResponseEntity<Pedido> inserirNovoPedido(@RequestBody Pedido codpedido) {
	
try {
			
	Pedido registroInserido = service.criarPedido(codpedido);
			
	if(registroInserido != null) {
				return new ResponseEntity<Pedido>(registroInserido, HttpStatus.CREATED);
			} else {
				return new ResponseEntity<Pedido>(HttpStatus.UNAUTHORIZED);
			}

		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Pedido>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
	
	@RequestMapping("/pedido")
	public ResponseEntity<List<Pedido>> listarPedido() {
		return new ResponseEntity<List<Pedido>>(service.ListarPedidos(), HttpStatus.OK);

	}
	
	@RequestMapping("/exclusao/{id}")
	public ResponseEntity<Pedido> deletePedido(@PathVariable(value = "id") int codpedido) {
		service.deletarPedido(codpedido);
		return new ResponseEntity<Pedido>(HttpStatus.OK);
	}
	
	
	@RequestMapping("/up")
	public ResponseEntity<Pedido> updatePedido(@RequestBody Pedido codpedido) {
		service.atualizarPedido(codpedido);
		return new ResponseEntity<Pedido>(HttpStatus.OK);
	}
	

}
