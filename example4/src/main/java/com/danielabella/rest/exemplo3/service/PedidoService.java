package com.danielabella.rest.exemplo3.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.danielabella.rest.exemplo3.domain.Pedido;
import com.danielabella.rest.exemplo3.repository.PedidoRepository;

/**
 * 
 * @author Mateus
 *
 *
 */

@Service
public class PedidoService {
	
	@Autowired
	private PedidoRepository pedidorepository;
	
	
	//get
		public  List<Pedido>ListarPedidos() {
			return pedidorepository.findAll();
			
		}
		//get
		public Pedido obterPedidoPorCodigo(int codigo_pd) {
			return pedidorepository.findOne(codigo_pd);
		}
		
		//post implementar
		
		public Pedido criarPedido(Pedido pedido) {
			return pedidorepository.save(pedido);
		}

		//put 
		
		public void atualizarPedido(Pedido pedido) {
			pedidorepository.save(pedido);
		}
		
		//deletar
		
		public void deletarPedido(int id_pd) {
			pedidorepository.delete(id_pd);
		}

}

