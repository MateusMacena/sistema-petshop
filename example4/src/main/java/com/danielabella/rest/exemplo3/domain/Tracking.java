package com.danielabella.rest.exemplo3.domain;

import java.sql.Timestamp;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
*
* @author Mateus
*
*/

@Entity
public class Tracking {
	
	
	private int cod_tracking;
	private int cod_pedido;
	private int cod_status;
	private String descricao;
	private Timestamp data_status;
	private String geo_lat;
	private String geo_long;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getCod_tracking() {
		return cod_tracking;
	}
	public void setCod_tracking(int cod_tracking) {
		this.cod_tracking = cod_tracking;
	}
	public int getCod_pedido() {
		return cod_pedido;
	}
	public void setCod_pedido(int cod_pedido) {
		this.cod_pedido = cod_pedido;
	}
	public int getCod_status() {
		return cod_status;
	}
	public void setCod_status(int cod_status) {
		this.cod_status = cod_status;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public Timestamp getData_status() {
		return data_status;
	}
	public void setData_status(Timestamp data_status) {
		this.data_status = data_status;
	}
	public String getGeo_lat() {
		return geo_lat;
	}
	public void setGeo_lat(String geo_lat) {
		this.geo_lat = geo_lat;
	}
	public String getGeo_long() {
		return geo_long;
	}
	public void setGeo_long(String geo_long) {
		this.geo_long = geo_long;
	}
	
	
}
